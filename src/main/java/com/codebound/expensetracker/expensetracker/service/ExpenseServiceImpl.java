package com.codebound.expensetracker.expensetracker.service;

import java.util.List;

import com.codebound.expensetracker.expensetracker.model.Expense;
import com.codebound.expensetracker.expensetracker.repository.ExpenseRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExpenseServiceImpl implements ExpenseService {

    @Autowired
    ExpenseRepo expenseRepo;

    @Override
    public List<Expense> findAll() {
        return expenseRepo.findAll();
    }

    @Override
    public Expense save(Expense expense) {
        expenseRepo.save(expense);
        return expense;
    }

    @Override
	public Expense findById(Long id) {
		if(expenseRepo.findById(id).isPresent()){
			return expenseRepo.findById(id).get();
		}
		return null;
    }
    
    @Override
	public void delete(Long id) {
		Expense expense = findById(id);
		expenseRepo.delete(expense);
	}

    
}
