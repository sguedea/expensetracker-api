package com.codebound.expensetracker.expensetracker.service;

import java.util.List;

import com.codebound.expensetracker.expensetracker.model.Expense;

public interface ExpenseService {

    List<Expense> findAll();
    
    Expense save(Expense expense);

    Expense findById(Long id);

    void delete(Long id);

}
